// Generated by CoffeeScript 1.10.0

/*
  var greet;
  greet = function (name) {
    return 'hello' + name + '!';
  }
  
  function greet (name) {
    return 'hello' + name + '!';
  } // в CoffeeScript нет аналога.
 */

(function() {
  var func, greet, normalize, outer,
    slice = [].slice;

  greet = function(name) {
    updateSomething();
    changeSomething();
    "Hello, " + name + "!";
    return processSomething();
  };

  greet = function(name) {
    if (name) {
      return 'hello';
    } else {
      return 'goodbye';
    }
  };

  greet = function(name) {
    return 0;
  };

  greet = function() {
    return "Hello";
  };

  greet = function(name) {};

  greet = function(name) {
    if (name == null) {
      name = 'melihovv';
    }
    return "Hello, " + name + "!";
  };

  normalize = function() {
    var length, vectors;
    length = arguments[0], vectors = 2 <= arguments.length ? slice.call(arguments, 1) : [];
  };

  outer = 10;

  func = function() {
    var inner;
    outer = 20;
    inner = 11;
  };

  update(object);

  show(update(object));

  show(update(object, 10, 20));

  show(update(object, 10, 20), false);

  func();

  func();

  a(+b);

  (function() {
    return something();
  })();

}).call(this);

//# sourceMappingURL=functions.js.map
