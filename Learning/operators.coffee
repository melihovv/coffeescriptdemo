first && second
first and second

first || second
first or second

be or not toBe

a == b
a != b
a is b
a isnt b

a isnt b
a is not b

update a if 10 < a < 20 < b < c

[a, b] = [b, a];
[book, author, pages] = ['1984', 'G. Orwell', 327]

car =
  make: 'Porsche'
  model: 911
  engine:
    type: 'V8'
    hp: 478
  cost: 60000

{model, engine: {type}} = car

user =
  name: 'Melihovv'
  age: 20
  lastVisit: do Date.now

'age' of user

range = [10..20]
14 in range

country in ['Russia', 'USA']

book = 2323
open book if book
# Проверка на null и undefined.
# Также проверка на функцию.
open? book if book?

object = {}
object.speed = 100
object.speed ?= 100
# Если скорость не может быть нулевой.
object.speed ||= 100
object.speed or= 1000

this.age = config.age || 10
this.age = config.age ? 10
