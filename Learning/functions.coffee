###
  var greet;
  greet = function (name) {
    return 'hello' + name + '!';
  }
  
  function greet (name) {
    return 'hello' + name + '!';
  } // в CoffeeScript нет аналога.
###

#greet = (name) ->
#  "Hello, #{name}!"
# -> <=> function
# Список аргументов до ->
# Возвращается последняя инструкция.
greet = (name) ->
  do updateSomething
  do changeSomething
  "Hello, #{name}!"
  do processSomething

greet = (name) ->
  if name
    'hello'
  else
    'goodbye'

greet = (name) ->
  return 0

# Функция без аргументов.
greet = -> "Hello"

# Возвращает undefined.
greet = (name) ->
  return

greet = (name = 'melihovv') ->
  return "Hello, #{name}!"

normalize = (length, vectors...) ->

outer = 10;
func = ->
  outer = 20
  inner = 11
  return

# Вызов функции.
update object
show update object
show update object, 10, 20
show update(object, 10, 20), false

# Вызов без аргументов.
func()
do func

# Неожиданно: вызов функции.
a +b

# Анонимные функции.
do -> do something
