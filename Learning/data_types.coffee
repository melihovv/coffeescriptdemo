window.myVar = 'hello'

# one line comment
###
  multi line comment
###
  
thisIsVar = 0

bool = true # false on off yes no

string =
"
haha
baga
"

singleQuoteString =
'one
two
three'

# May be single quotes. Then string will be as is.
hereDocString = """
one
two
three
"""

name="melihovv"
greeting = "hello, #{name.toUpperCase()}"

array = [
  1, 2, 3
  4, 5, 6
  7, 8, 9
]

range = [0..10]
rangeExclusive = [0...10]

rangeMore20 = [0..25]
rangeSlice[5..10]
rangeSlice[..10]
rangeSlice[5..]
rangeSlice[..]

user =
  name: 'Joe Doe'
  age: 24
  status: 'Web Developer'
  skills:
    html: 10
    css: 8
    js: 9

visaRegEx = ///
    ^4 # Starts with 4
    [0-9]{12} # 12 digits
    (?:[0-9]{3})?$ # New cards have 16 digits
///
