while isReady
  do something

while isReady then do something
do something while isReady

while not isReady
  do prepare

until isReady
  do prepare

do prepare until isReady

for i in [0..10]
  update i
  
for i in [0...10]
  update i

for i in [0...10] then update i
update i for i in [0...10]

update i for i in [0...10] by 4

console.log update i for i in [0..100] when isPrime i
console.log update i for i in [0..100] by 4 when isPrime i
console.log update i for i in [0..100] when isPrime i by 4

primeNumbers = (i for i in [0..100] when isPrime i by 4)

show element for element in array when element in rightElements
remove index for element, index in array when element in rightElements

console.log property for property of object
console.log object[property] for property of object
console.log value for property, value of object
console.log value for own property, value of object

for event in ['click', 'mouseover', 'mouseout', 'focus']
  do (even) ->
    mySuperLibrary::["on#{event}"] = (callback) ->
      mySuperLibrary::on event, callback
      return
    return
