if isReady
  activate something
  if not isFull
    append something
else
  do prepare

if isReady then activate something
activate something if isReady

# if not <=> unless
unless isReady then activate something
activate something unless isReady

action = if mode is 'idle' then 'sleep' else 'wander'
show if message then message else warning

#switch state
#  when 0 then message = 'request not initialized'
#  when 1
#    message = 'server connection established'
#  when 2 then message = 'request recieved'
#  when 3 then message = 'processing request'
#  when 4 then message = 'request finished and response is ready'
#  else message = 'unknown state'
message = switch state
  when 0 then 'request not initialized'
  when 1 then 'server connection established'
  when 2 then 'request recieved'
  when 3 then 'processing request'
  when 4 then 'request finished and response is ready'
  else message = 'unknown state'

console.log  "Message: [#{state}] #{switch state
  when 0 then 'request not initialized'
  when 1 then 'server connection established'
  when 2 then 'request recieved'
  when 3 then 'processing request'
  when 4 then 'request finished and response is ready'
  else message = 'unknown state'
}"
