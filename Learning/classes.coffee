class Person
#  constructor: (name, age) ->
#    @name = name
#    @age = age
  constructor: (@name, @age) ->

  getInfo: ->
    "Name: #{name}, Age: #{age}"
  
  @myStaticFunc: ->
    do something
  
  @myStaticVal: 12345

class WebDeveloper extends Person
  constructor: ->
    super 10, 20, 30
    do something
  
  getInfo: ->
    super arg1, arg2
    'My custom getInfo'

  #var that = this;
  #var self = this;
  #var _this = this;
  myMethod: ->
    someFunc =>
      process @age

Person::myMethod = -> say 'hi'
